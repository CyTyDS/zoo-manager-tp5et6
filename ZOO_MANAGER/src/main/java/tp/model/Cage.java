package tp.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Collection;

@XmlRootElement
public class Cage {
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({
	"name",
	"position",
	"capacity",
	"residents"
	})
    /**
     * The name of this cage
     */
	@JsonProperty("name")
    private String name;
    /**
     * The visitor entrance location
     */
	@JsonProperty("position")
    private Position position;
    /**
     * The maximum number of animals in this cage
     */
	@JsonProperty("capacity")
    private Integer capacity;
    /**
     * The animals in this cage.
     */
	@JsonProperty("residents")
    private Collection<Animal> residents;

    public Cage() {
    }

    public Cage(String name, Position position, Integer capacity, Collection<Animal> residents) {
        this.name = name;
        this.position = position;
        this.capacity = capacity;
        this.residents = residents;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("capacity")
    public Integer getCapacity() {
        return capacity;
    }

    @JsonProperty("position")
    public Position getPosition() {
        return position;
    }

    @JsonProperty("residents")
    public Collection<Animal> getResidents() {
        return residents;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("capacity")
    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    @JsonProperty("position")
    public void setPosition(Position position) {
        this.position = position;
    }

    @JsonProperty("residents")
    public void setResidents(Collection<Animal> residents) {
        this.residents = residents;
    }
}
