package tp.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;

@XmlRootElement
public class Center {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({
	"_id",
	"cages",
	"position",
	"name"
	})
	
	
	@SuppressWarnings("unused")
	@JsonProperty("_id")
	private String _id = "center";
	@JsonProperty("cages")
    Collection<Cage> cages;
	@JsonProperty("position")
    Position position;
	@JsonProperty("name")
    String name;

    public Center() {
        cages = new LinkedList<>();
    }

    public Center(Collection<Cage> cages, Position position, String name) {
        this.cages = cages;
        this.position = position;
        this.name = name;
    }
    
    @JsonProperty("_id")
    public String get_id() {
		return _id;
	}

    @JsonProperty("_id")
	public void set_id(String _id) {
		this._id = _id;
	}

    public Animal findAnimalById(UUID uuid) throws AnimalNotFoundException {
        return this.cages.stream()
                .map(Cage::getResidents)
                .flatMap(Collection::stream)
                .filter(animal -> uuid.equals(animal.getId()))
                .findFirst()
                .orElseThrow(AnimalNotFoundException::new);
    }

    @JsonProperty("cages")
    public Collection<Cage> getCages() {
        return cages;
    }

    @JsonProperty("position")
    public Position getPosition() {
        return position;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("cages")
    public void setCages(Collection<Cage> cages) {
        this.cages = cages;
    }

    @JsonProperty("position")
    public void setPosition(Position position) {
        this.position = position;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }
}
