package tp.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.UUID;

@XmlRootElement
public class Animal {
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({
	"name",
	"cage",
	"species",
	"id"
	})
	
    /**
     * Name of the animal
     */
	@JsonProperty("name")
    private String name;

    /**
     * The place where it currently live in.
     */
	@JsonProperty("cage")
    private String cage;
    /**
     * The animal's species
     */
	@JsonProperty("species")
    private String species;
    /**
     * The animal unique identifier.
     */
	@JsonProperty("id")
    private UUID id;

    public Animal() {
    }

    public Animal(String name, String cage, String species, UUID id) {
        this.name = name;
        this.cage = cage;
        this.species = species;
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("<%s : %s ( %s ) is a %s currently in %s>", this.getClass().getName(), name, id, species, cage);
    }

    @JsonProperty("id")
    public UUID getId() {
        return id;
    }

    @JsonProperty("cage")
    public String getCage() {
        return cage;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("species")
    public String getSpecies() {
        return species;
    }

    @JsonProperty("id")
    public void setId(UUID id) {
        this.id = id;
    }

    @JsonProperty("cage")
    public void setCage(String cage) {
        this.cage = cage;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }
    
    @JsonProperty("species")
    public void setSpecies(String species) {
        this.species = species;
    }
}
