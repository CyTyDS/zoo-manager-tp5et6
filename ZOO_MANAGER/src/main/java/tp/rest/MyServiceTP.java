package tp.rest;

import tp.model.*;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.xml.bind.JAXBException;
import javax.xml.ws.http.HTTPException;

import org.bson.Document;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.UUID;

@Path("/zoo-manager/")
public class MyServiceTP {

    private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");
    private MongoCollection<BasicDBObject> collection;
    
    public MyServiceTP() {
		MongoClientURI uri = new MongoClientURI(
		    "mongodb+srv://admin:admin@archidist-3nllk.mongodb.net/test?retryWrites=true");
		MongoClient mongoClient = new MongoClient(uri);
		MongoDatabase db = mongoClient.getDatabase("ArchiDist");
		this.collection = db.getCollection("ArchiDist", BasicDBObject.class);
		//mongoClient.close();
		
    	loadFile();
    	
    	/*
        // Fill our center with some animals
        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(
                        new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );

        center.getCages().addAll(Arrays.asList(usa, amazon));
        */
    }
    
    /**
     * GET method bound to calls on /center/journey/from/{position}
     */
    @GET
    @Path("/center/journey/from/{position}")
    @Produces("application/xml")
    public String centerJourneyFrom(@PathParam("position") String position) throws JAXBException {
		//Fait des choses avec l'API GraphHopper
		String[] splitted = position.split(",");
		String lat = splitted[0];
		String lon = splitted[1];
		
		//API CALL
        Client client = javax.ws.rs.client.ClientBuilder.newClient();
        return client.target("https://graphhopper.com/api/1/route?point=" + this.center.getPosition().getLatitude()
                + "," + center.getPosition().getLongitude() + "&point=" + lat + "," + lon
                + "&vehicle=car&locale=fr&key=f9a7c76b-3bcf-4ae1-9ccb-33a922fe0334&type=gpx")
        		.request()
        		.get(String.class);
    }
    
    /**
     * GET method bound to calls on /find/byName/{attr}
     */
    @GET
    @Path("/find/byName/{attr}")
    @Produces("application/xml")
    public Animal findByNameAnimal(@PathParam("attr") String attr) throws JAXBException {
    	try {
			Animal ret = this.center.getCages()
			.stream()
			.map(Cage::getResidents)
			.flatMap(Collection::stream)
			.filter(animal -> attr.equals(animal.getName()))
			.findFirst()
			.orElseThrow(AnimalNotFoundException::new);
			
            return ret;
		} catch (AnimalNotFoundException e) {
			// Auto-generated catch block
			e.printStackTrace();
			throw new HTTPException(405);
		}
    }
    
    /**
     * GET method bound to calls on /find/at/{attr}
     */
    @GET
    @Path("/find/at/{attr}")
    @Produces("application/xml")
    public Cage findAtAnimal(@PathParam("attr") String attr) throws JAXBException {
		//Création de la position via la string attr
		String[] splitted = attr.split(",");
		String lat = splitted[0];
		String lon = splitted[1];
		Position posToFind = new Position(new Double(lat), new Double(lon));
		
		//Recherche
		for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
    		Cage cage = it.next();
    		if (cage.getPosition().equals(posToFind)) {
    			return cage;
    		}
    	}
        throw new HTTPException(404);
    }
    
    /**
     * GET method bound to calls on /find/near/{attr}
     */
    @GET
    @Path("/find/near/{attr}")
    @Produces("application/xml")
    public Cage findNearAnimal(@PathParam("attr") String attr) throws JAXBException {
		//Création de la position via la string attr
		String[] splitted = attr.split(",");
		String lat = splitted[0];
		String lon = splitted[1];
		Position posToFind = new Position(new Double(lat), new Double(lon));
		
		//Recherche
		for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
    		Cage cage = it.next();
    		if (Math.abs(cage.getPosition().getLatitude() - posToFind.getLatitude()) <= 1
    				&& Math.abs(cage.getPosition().getLongitude() - posToFind.getLongitude()) <= 1) {
    			return cage;
    		}
    	}
		throw new HTTPException(404);
	}
    
    /**
     * GET method bound to calls on /animals/{something}/wolf
     */
    @GET
    @Path("/animals/{id}/wolf")
    @Produces("application/xml")
    public String getWolfram(@PathParam("id") String animal_id) throws JAXBException {
		//Faire des choses avec l'API Wolfram
		//https://api.wolframalpha.com/v2/query?input=[wordToSearch]&output=XML&appid=QJYW5T-8492R6VU39
		
		//GET ANIMAL_ID
		Animal animal;
		try {
			animal = this.center.findAnimalById(UUID.fromString(animal_id));
		} catch (AnimalNotFoundException e) {
			// Auto-generated catch block
			e.printStackTrace();
			throw new HTTPException(404);
		}
		
		//API CALL
        Client client = javax.ws.rs.client.ClientBuilder.newClient();
        return client.target("https://api.wolframalpha.com/v2/query?input=" + animal.getSpecies().replaceAll(" ", "%20") + "&output=XML&appid=QJYW5T-8492R6VU39")
        		.request()
        		.get(String.class);
    }
    
    /**
     * GET method bound to calls on /animals/{something}
     */
    @GET
    @Path("/animals/{id}/")
    @Produces("application/xml")
    public Animal getAnimal(@PathParam("id") String animal_id) throws JAXBException {
        try {
            return center.findAnimalById(UUID.fromString(animal_id));
        } catch (AnimalNotFoundException e) {
            throw new HTTPException(404);
        }
    }
    
    /**
     * POST method bound to calls on /animals/{something}
     */
    @POST
    @Path("/animals/{id}/")
    @Consumes({"application/xml", "application/json" })
    public Center postAnimal(Animal animal, @PathParam("id") String animal_id) throws JAXBException {
    	animal.setId(UUID.fromString(animal_id));
        this.center.getCages()
                .stream()
                .filter(cage -> cage.getName().equals(animal.getCage()))
                .findFirst()
                .orElseThrow(() -> new HTTPException(404))
                .getResidents()
                .add(animal);
        
        writeFile();
        return this.center;
    }
    
    /**
     * PUT method bound to calls on /animals/{something}
     */
    @PUT
    @Path("/animals/{id}/")
    @Consumes({"application/xml", "application/json" })
    public Center putAnimal(Animal toAdd, @PathParam("id") String animal_id) throws JAXBException {
    	try {
	    	Animal toDelete = center.findAnimalById(UUID.fromString(animal_id));
			this.center.getCages()
			.stream()
			.filter(cage -> cage.getName().equals(toDelete.getCage()))
	        .findFirst()
	        .orElseThrow(() -> new HTTPException(404))
	        .getResidents()
	        .remove(toDelete);
			
			//...puis on ajoute le nouveau
			toAdd.setId(UUID.fromString(animal_id));
			this.center.getCages()
	        .stream()
	        .filter(cage -> cage.getName().equals(toAdd.getCage()))
	        .findFirst()
	        .orElseThrow(() -> new HTTPException(404))
	        .getResidents()
	        .add(toAdd);
			
			writeFile();
	        return this.center;
    	} catch (AnimalNotFoundException e) {
			// Auto-generated catch block
			e.printStackTrace();
			throw new HTTPException(400);
		}
    }
    
    /**
     * DELETE method bound to calls on /animals/{something}
     */
    @DELETE
    @Path("/animals/{id}/")
    @Produces("application/xml")
    public Center deleteAnimal(@PathParam("id") String animal_id) throws JAXBException {
    	try {
			Animal animal = center.findAnimalById(UUID.fromString(animal_id));
			Collection<Cage> cages = this.center.getCages();
			cages
			.stream()
			.filter(cage -> cage.getName().equals(animal.getCage()))
            .findFirst()
            .orElseThrow(() -> new HTTPException(404))
            .getResidents()
            .remove(animal);
			this.center.setCages(cages);
			
			writeFile();
			return this.center;
		} catch (AnimalNotFoundException e) {
			// Auto-generated catch block
			e.printStackTrace();
			throw new HTTPException(400);
		}
    }

    /**
     * GET method bound to calls on /animals
     */
    @GET
    @Path("/animals/")
    @Produces("application/xml")
    public Center getAnimals(){
        return this.center;
    }

    /**
     * POST method bound to calls on /animals
     */
    @POST
    @Path("/animals/")
    @Consumes({"application/xml", "application/json" })
    public Center postAnimals(Animal animal) throws JAXBException {
    	animal.setId(UUID.randomUUID());
        this.center.getCages()
                .stream()
                .filter(cage -> cage.getName().equals(animal.getCage()))
                .findFirst()
                .orElseThrow(() -> new HTTPException(404))
                .getResidents()
                .add(animal);
        
        writeFile();
        return this.center;
    }
    
    /**
     * PUT method bound to calls on /animals
     */
    @PUT
    @Path("/animals/")
    @Consumes({"application/xml", "application/json" })
    public Center putAnimals(Cage newCage) throws JAXBException {
    	//DELETE
    	for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
    		Cage cage = it.next();
    		cage.setResidents(new LinkedList<>());
    	}
    	//POST
    	for (Animal animal : newCage.getResidents()) {
    		animal.setId(UUID.randomUUID());
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
    	}
    	
    	writeFile();
        return this.center;
    }
    
    /**
     * DELETE method bound to calls on /animals
     */
    @DELETE
    @Path("/animals/")
    public Center deleteAnimals() throws JAXBException {
    	for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
    		Cage cage = it.next();
    		cage.setResidents(new LinkedList<>());
    	}
    	
    	writeFile();
        return this.center;
    }
    
    /**
     * POST method bound to calls on /cages
     * Add new empty cage
     */
    @POST
    @Path("/cages/")
    @Consumes({"application/xml", "application/json" })
    public Center addCage(Cage newCage) throws JAXBException {
    	if (newCage.getResidents() == null) {
    		newCage.setResidents(new LinkedList<>());
    	}
    	for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
    		Cage cage = it.next();
    		if (cage.getName().equals(newCage.getName())) {
    			return this.center;
    		}
    	}
        this.center.getCages().addAll(Arrays.asList(newCage));
        
        writeFile();
        return this.center;
    }
    
    /**
     * DELETE method bound to calls on /cages/{cageName}
     * Empty a cage
     */
    @DELETE
    @Path("/cages/{cageName}")
    public Center emptyCage(@PathParam("cageName") String cageName) throws JAXBException {
    	for (Iterator<Cage> it = this.center.getCages().iterator(); it.hasNext();) {
    		Cage cage = it.next();
    		if (cageName.equals(cage.getName())) {
    			cage.setResidents(new LinkedList<>());
    		}
    	}
    	
    	writeFile();
        return this.center;
    }
    
    private void loadFile() {
    	FindIterable<BasicDBObject> docs = collection.find();
    	
    	String value = "";
    	if (docs == null) {
    		return;
    	}
    	for (BasicDBObject doc : docs) {
    		value = (String) doc.get("value");
    	}
    	
    	if (value == null || value.equals("")) {
    		return;
    	}
		
		//JSON from String to Object
		ObjectMapper om = new ObjectMapper();
    	try {
			this.center = om.readValue(value, Center.class);
		} catch (Exception e) {
			//Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void writeFile() {
		//Object to JSON in String
    	ObjectMapper om = new ObjectMapper();
    	String value = "";
		try {
			value = om.writeValueAsString(this.center);
		} catch (JsonProcessingException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Document searchQuery = new Document();
    	searchQuery.put("_id", "center");
		
    	Document document = new Document();
    	document.put("value", value);
    	
    	Document updateObject = new Document();
    	updateObject.put("$set", document);
    	 
    	this.collection.updateOne(searchQuery, updateObject);
    }
}
