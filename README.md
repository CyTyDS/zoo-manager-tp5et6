# Zoo Manager TP5et6

 * Le rapport du projet correspond au fichier [Rapport.pdf](Rapport.pdf)
 * L'**adresse publique** du service déployé en PaaS est disponible dans le fichier [AdressePaaS.txt](AdressePaaS.txt)
 * Le **code de notre service RESTFUL** est disponible dans le dossier [ZOO_MANAGER](ZOO_MANAGER). Il y a dans ce dossier le code source de l'application, mais aussi le fichier manifest.yml donné dans le sujet, et des scripts shells afin d'automatiser les builds de l'application et le lancement du serveur Apache Tomcat
 * Nous disposons d'un dossier contenant un **animal et une cage de test** sous format JSON, disponible dans le dossier [JSON Objects](JSON Objects)

-------------------------

# Le service ZOO_MANAGER

Notre service REST dispose des fonctionnalités suivantes : 

 * PUT		/animals					?????? -> Cage (expliqué dans le rapport du premier projet)
 * DELETE	/animals
 * POST		/animals/{animal_id}		Animal
 * PUT		/animals/{animal_id}		Animal
 * DELETE	/animals/{animal_id}
 * GET		/find/byName/{name}
 * GET		/find/at/{pos}
 * GET		/find/near/{pos}
 * GET		/animals/{animal_id}/wolf
 * GET		/center/journey/from/{pos}

En plus des méthodes demandées pour le projet, j'ai rajouté d'autres méthodes utiles : 

 * POST		/cages					Cage	Permet d'ajouter une nouvelle cage à notre centre
 * DELETE	/cages/{cageName}				Permet de vider une cage de notre centre, référencée par son nom
